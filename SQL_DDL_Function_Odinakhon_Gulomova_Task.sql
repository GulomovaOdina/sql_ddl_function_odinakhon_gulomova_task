CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT 
    fc.category_name,
    SUM(fp.payment_amount) AS total_sales_revenue
FROM 
    film_category fc
INNER JOIN 
    film_sales fs ON fc.film_id = fs.film_id
INNER JOIN
    film_payments fp ON fs.sale_id = fp.sale_id
WHERE 
    DATE_TRUNC('quarter', CURRENT_DATE) = DATE_TRUNC('quarter', fs.sale_date)
GROUP BY 
    fc.category_name
HAVING 
    SUM(fp.payment_amount) > 0;

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter DATE)
RETURNS TABLE(category_name VARCHAR, total_sales_revenue NUMERIC) AS $$
BEGIN
    RETURN QUERY 
    SELECT 
        fc.category_name,
        SUM(fp.payment_amount) AS total_sales_revenue
    FROM 
        film_category fc
    INNER JOIN 
        film_sales fs ON fc.film_id = fs.film_id
    INNER JOIN
        film_payments fp ON fs.sale_id = fp.sale_id
    WHERE 
        DATE_TRUNC('quarter', current_quarter) = DATE_TRUNC('quarter', fs.sale_date)
    GROUP BY 
        fc.category_name
    HAVING 
        SUM(fp.payment_amount) > 0;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION new_movie(movie_title VARCHAR)
RETURNS VOID AS $$
DECLARE
    new_language_id INT;
BEGIN
    -- Check if the language exists
    SELECT language_id INTO new_language_id FROM language WHERE name = 'Klingon';
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language not found.';
    END IF;

    -- Insert the new movie
    INSERT INTO film (title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE)::INTEGER, new_language_id);
END;
$$ LANGUAGE plpgsql;
